const { response } = require("express");
const { suma, resta } = require('../helpers/myFunctions')

const getSuma = (req, res = response) => {
  const { a, b } = req.body;
  if ( a != null && b != null) {
    res.status(200).send({
      ok: true,
      msg: `La suma es ${suma( a, b )}`,
    });
  } else {
    res.status(400).send({
      ok: false,
      msg: "Hay un valor incorrecto",
    });
  }
};

const getResta = (req, res = response) => {
    const { a, b } = req.body;
    if ( a != null && b != null) {
      res.status(200).send({
        ok: true,
        msg: `La resta es ${resta( a, b )}`,
      });
    } else {
      res.status(400).send({
        ok: false,
        msg: "Hay un valor incorrecto",
      });
    }
};

module.exports = {
  getSuma,
  getResta,
};
