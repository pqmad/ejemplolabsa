const {Router} = require('express');
const {getSuma, getResta} = require('../controller/example.controller');

const router = Router();

router.post('/suma', getSuma );
router.post('/resta', getResta );

module.exports=router;
