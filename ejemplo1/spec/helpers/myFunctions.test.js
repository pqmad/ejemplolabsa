const { suma, resta } = require('../../helpers/myFunctions')

// LAS PRUEBAS SE EJECUTAN EN BASE A LAS 3A:
// ARRANGE
// ACT
// ASSERT

describe('Prueba sobre funcion suma:', () => {

    it('debería devolver la suma = 30', async () => {
        // ARRANGE info que se usara
        const a = 10;
        const b = 20;
        // ACT - donde nosotros hacemos uso de loq ue vamos a probar con la info del arrange
        const resultado = suma(a,b);
        // ASSERT - verificar
        expect(resultado).toBe(30);
    });

});

describe('Prueba sobre funcion resta:', () => {

    it('debería devolver la resta = -10', async () => {
        //ARRANGE
        const a = 10;
        const b = 20;
        // ACT
        const resultado = resta(a,b);
        //ASSERT
        expect(resultado).toBe(-10);
    });

});
