const { response } = require('express');
const { getEvents, createEvent } = require('../helpers/db-event-actions');

const getEventsByUserController = async( req, res = response ) => {
    const { uid } = req;
    const events = await getEvents(uid)

    if (events) {
        return res.status(200).send({
            ok: true,
            msg: 'Eventos obtenidos exitosamente',
            events
            
        })
        
    }
    return res.status(500).send({
        ok: false,
        msg: 'Ha ocurrido un error',
    })

}

const createEventController = async( req, res = response ) => {

    const { title, notes, start, end } = req.body;
    const {uid} = req;
    const newEvent = await createEvent(title, notes, start, end, uid);

    if (newEvent) {
        return res.status(200).send({
            ok: true,
            msg: 'Evento creado exitosamente',
            newEvent: newEvent.user.dataValues,
        })
        
    }
    return res.status(500).send({
        ok: false,
        msg: 'Ha ocurrido un error',
    })

}

module.exports = {
    getEventsByUserController,
    createEventController,
}