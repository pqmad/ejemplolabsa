const { DataTypes } = require('sequelize');
const { db } = require('../database/connection')

const Usuario = db.define('USUARIO', {
    uid: {
        type: DataTypes.NUMBER,
        primaryKey: true,
        autoIncrement: true
    },
    passwd: {
        type: DataTypes.STRING
    },
    name: {
        type: DataTypes.STRING
    },
    email: {
        type: DataTypes.STRING
    }
}, {
    tableName: 'USUARIO'
});

module.exports = {
    Usuario
}