const { Router } = require('express')
//IMPORTAR LOS CONTROLLERS
const { getEventsByUserController, createEventController } = require('../controller/events.controller');
// EXPRESS-VALIDATOR
const { check } = require('express-validator');
const { isDate } = require('../helpers/isDate');
// IMPORTAR VALIDADOR DE ERRORESS
const { validateFields } = require('../middlewares/validate-fields');
const { validateJWT } = require('../middlewares/validate-jwt')

const router = Router();

router.use(validateJWT)

router.get('/', getEventsByUserController)

router.post('/create-event', [
    check('title', 'El título es obligatorio').not().isEmpty(),
    check('start', 'Fecha de inicio es obligatorioa').custom(isDate),
    check('end', 'Fecha de finalización es obligatorioa').custom(isDate),
    validateFields,
], createEventController );

module.exports = router