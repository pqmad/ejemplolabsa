const { response } = require("express");
const { getUser, createUser } = require("../helpers/db-auth-actions");
const { generateJWT } = require("../helpers/generate-jwt");

const loginController= async (req, res=response) => {
    const {email, passwd} = req.body;
    
    const user= await getUser(email, passwd);
    if(user){
        const token= await generateJWT({...user});
        return res.status(200).send({
            ok: true,
            msg: 'Usuario logueado correctamente',
            user,
            token
        })
    }
    return res.status(500).send({
        ok: false,
        msg: 'Usuario o contraseña incorrectos'
    })

}
const createUserController= async (req, res=response) => {
    const {name, email, passwd} = req.body;
    const {ok, user}= await createUser(passwd, name, email);
    if(ok){
        const token= await generateJWT({...user});
        return res.status(200).send({
            ok,
            msg: 'Usuario creado correctamente',
            user,
            token
        })
    }
    return res.status(500).send({
        ok,
        msg: 'El usuario ya existe'
    })
}


const revalidateToken = async(req, res = response) => {
    const { uid, name, email } = req;
    const token = await generateJWT({uid, name, email})
    return res.json({
        ok: true,
        user: {
            uid, 
            name,
            email
        },
        token
    })
}

module.exports={
    loginController,
    createUserController,
    revalidateToken
}