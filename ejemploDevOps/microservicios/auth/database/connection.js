// const path = require('path');
// const dotenv = require('dotenv');
// dotenv.config({ path: path.resolve(__dirname, '../.env/.env') });

const {Sequelize} = require('sequelize');

const db= new Sequelize(process.env.DB_NAME,
    
    process.env.DB_USER,
    process.env.DB_PASS, 
    {
        host: process.env.DB_HOST,
        dialect: 'mysql',
        define:{
            timestamps: false
        }
    })
const dbConnection= async()=>{
    try {
        await db.authenticate();
        console.log('Database online');
    } catch (error) {
        throw new Error(error);
    }
}

module.exports={
    dbConnection,
    db
}