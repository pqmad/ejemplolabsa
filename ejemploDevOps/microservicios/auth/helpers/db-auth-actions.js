const Usuario = require("../models/user.model");

const getUser= async (emailParam="", passwdParam="") => {
    try {
        const user= await Usuario.findOne({
            where:{
                email: emailParam,
                passwd: passwdParam
            }
        });
        const {passwd: pass, ...rest}= user.dataValues;
        return{
            ...rest
        }
    } catch (error) {
        console.log(error);
    }
}


const createUser= async (passwd = '', name = '', email = '') => {
    try{
        const find_user= await Usuario.findOne({
            where:{
                email
            }
        });
        if(find_user){
            return {
                ok: false,
                msg: 'El usuario ya existe'
            }
        }
        const user= await Usuario.create({
            passwd,
            name,
            email
        });
        const {passwd: pass, ...rest}= user.dataValues;
        return{
            ok: true,
            user: {
                ...rest
            }
        }
    }catch(error){
        console.log(error);
    }
}


module.exports={
    getUser,
    createUser
}