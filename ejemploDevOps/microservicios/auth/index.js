'use strict'
//require('dotenv').config({path:__dirname+"/.env/.env"})
const express = require('express')
const cors = require('cors')
const {dbConnection} = require('./database/connection')
const {PORT} = process.env


const app = express()
app.use(express.static('public'))
app.use(cors())
app.use(express.json())
dbConnection()

app.use('/api/auth', require('./routes/auth.route'))


app.listen(PORT,()=>{
    console.log(`Server running on port ${PORT}`);
})