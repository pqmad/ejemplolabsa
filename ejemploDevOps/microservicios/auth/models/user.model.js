const { DataTypes, Sequelize } = require("sequelize");
const { db } = require("../database/connection");


const Usuario = db.define("USUARIO", {
    uid:{
        type: DataTypes.NUMBER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    passwd:{
        type: DataTypes.STRING,
        allowNull: false
    },
    name:{
        type: DataTypes.STRING,
        allowNull: false
    },
    email:{
        type: DataTypes.STRING,
        allowNull: false
    },
},
{
    tableName: "USUARIO"
})

module.exports=Usuario;