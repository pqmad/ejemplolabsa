const {Router} = require('express');
const { loginController, createUserController, revalidateToken } = require('../controller/auth.controller');
const { validateFields } = require('../middleware/validate-fields');
const { check } = require('express-validator');
const { validateJWT } = require('../middleware/validate-jwt');

const router = Router();

router.post('/login',
[
    check('email', 'El email es obligatorio y tiene que tener estructara de correo').notEmpty().isEmail(),
    check('passwd', 'La contraseña es obligatoria y tiene que ser una cadena de texto').notEmpty().isString(),
    validateFields
],
loginController )

router.get('/renew', validateJWT, revalidateToken)

router.post('/create-user', 
[
    check('name', 'El nombre es obligatorio y tiene que ser una cadena de texto').notEmpty().isString(),
    check('email', 'El email es obligatorio y tiene que tener estructara de correo').notEmpty().isEmail(),
    check('passwd', 'La contraseña es obligatoria y tiene que ser una cadena de texto').notEmpty().isString(),
    validateFields
], 
createUserController)

module.exports=router;