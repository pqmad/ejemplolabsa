const { getUser } = require("../../helpers/db-auth-actions");
require('jasmine')

describe('Pruebas sobre acciones de auth', () => {

    const helpers = {
        getUser
    }

    it('Debería obtener el usuario', async() => {
        // Arrange
        const data = {
            email: 'arianaprueba@algo.com', 
            name: 'Ariana'
        }
        spyOn(helpers, 'getUser').and.returnValue(data)
        // Act
        const resultado = helpers.getUser('arianaprueba@algo.com', 'USER12345');
        // Assert
        expect(resultado).toBe(data)
        expect(helpers.getUser).toHaveBeenCalled();

    });
});