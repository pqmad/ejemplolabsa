// const path = require('path');
// const dotenv = require('dotenv');
// dotenv.config({ path: path.resolve(__dirname, '../../.env/.env') });
require('jasmine')
const { generateJWT } = require("../../helpers/generate-jwt")


describe('Pruebas para generar un jwt', () => {
    it('Debería generar correctamente el jwt', async () => {
        // Arrange
        const userData = {
            uid: 1,
            name: 'Ariana',
            email: 'arianaprueba@algo.com'
        }
        // Act
        const jwt = await generateJWT({...userData});
        // Assert
        expect(jwt).not.toBeNull()
        expect(jwt.length).toBeGreaterThan(0);
    })
})