CREATE DATABASE sadevops;

USE sadevops;

CREATE TABLE USUARIO (
	uid int primary key not null auto_increment,
	passwd varchar(500) not null,
	name varchar(200) not null,
	email varchar(500) not null
)

-- CREATE TABLE EVENTO (
-- 	idevent int primary key not null auto_increment,
-- 	startDate varchar(200) not null,
-- 	endingDate varchar(200) not null,
-- 	title varchar(100) not null,
-- 	notes varchar(500) not null,
-- 	uid int not null,
-- 	foreign key (uid) references USUARIO(uid)
-- )

CREATE TABLE EVENTO (
	idevent int primary key not null auto_increment,
	startDate datetime not null,
	endingDate datetime not null,
	title varchar(100) not null,
	notes varchar(500) not null,
	uid int not null,
	foreign key (uid) references USUARIO(uid)
)
-- # INSERT INTO USUARIO (passwd, name, email) VALUES ('USER12345', 'USER1', 'algo1@algo.com');

-- # show tables;

