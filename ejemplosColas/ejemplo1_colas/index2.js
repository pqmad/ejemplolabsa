const amqp = require('amqplib');

const messageBrokerURL = 'amqp://myuser:mypassword@localhost:5672';

async function consumeMessages() {
  try {
    // Nombre de la cola a la que queremos escuchar y publicar
    const queue = 'mensajes';
    // Conexión a rabbitmq
    const connection = await amqp.connect(messageBrokerURL);
    const channel = await connection.createChannel();
    // Se asegura que la cola exista, si no existe la crea
    await channel.assertQueue(queue, { durable: false });
    console.log("Esperando mensajes...")
    // Consume de la cola, es decor, obtiene el elemento
    await channel.consume(queue, (data) => {
        console.log({"mensaje_recibido": data.content.toString()})
        // Reconoce el mensaje y al final lo elimina de la cola
        channel.ack(data);
    });
  } catch (error) {
    console.error('Error en el consumidor:', error);
  }
}

consumeMessages();