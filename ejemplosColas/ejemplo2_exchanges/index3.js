const amqp = require('amqplib');

const messageBrokerURL = 'amqp://myuser:mypassword@localhost:5672';

async function consumeMessages() {
  try {
    const connection = await amqp.connect(messageBrokerURL);
    const channel = await connection.createChannel();

    const exchange = 'mensaje-exchange';
    await channel.assertExchange(exchange, 'direct', { durable: true });

    const { queue } = await channel.assertQueue('micro3', { exclusive: false });
    channel.bindQueue(queue, exchange, 'tipo1');

    console.log('Esperando mensajes...');

    channel.consume(queue, (message) => {
      if (message !== null) {
        const mensaje = message.content.toString();
        console.log({"mensaje_recibido": mensaje.toString()});
        channel.ack(message);
      }
    });
  } catch (error) {
    console.error('Error en el consumidor:', error);
  }
}

consumeMessages();