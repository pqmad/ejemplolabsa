'use strict'
require('dotenv').config({ path:__dirname + '/.env/.env' });
const express = require('express');
const cors = require('cors')
// const amqp = require("amqplib");

const {PORT} = process.env;

// CREAR EL SERVIDOR
const app = express();

// CORS
app.use(cors())

// PARSEAR DEL BODY
app.use(express.json())

app.use('/api/micro1', require('./routes/micro1.route'))

app.listen(PORT, () => {
    console.log(`Servidor corriendo en puerto ${PORT}`)
})