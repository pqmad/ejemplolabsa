const { Router } = require('express')
const { newUserController } = require('../controller/micro1.controller')
// const { newUserController } = require('../controller/micro11.controller')

const router = Router();

router.post('/new-user', newUserController); // localhost:3000/api/micro1/new-user

module.exports = router;
